from common.json import ModelEncoder
from .models import Conference, Location
from presentations.models import Presentation

# from attendees.models import Attendee


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {"location": LocationListEncoder()}


class ListPresentationEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]


class DetailPresentationEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


# class AttendeeListEncoder(ModelEncoder):
#     model = Attendee
#     properties = ["name"]


# class AttendeeDetailEncoder(ModelEncoder):
#     model = Attendee
#     properties = [
#         "email",
#         "name",
#         "company_name",
#         "created",
#         "conference",
#     ]
#     encoders = {
#         "conference": ConferenceListEncoder(),
#     }
